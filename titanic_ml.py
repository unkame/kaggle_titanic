'''
Kaggle - Titanic case
Only use scikit learn
Final model - using VotingClassifier
Final score - 0.78462
'''

import numpy as np, pandas as pd
import re
from collections import Counter

from sklearn.model_selection import train_test_split
from sklearn.feature_extraction import DictVectorizer
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, \
                                GradientBoostingClassifier, VotingClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV


def transform_data(df, verbose=False):
    '''
    Filling NA and one-hot
    '''

    # method:
    # Pclass         one-hot
    # Name           removed
    # Sex            one-hot
    # Age            fill na by mean
    # SibSp          + Parch > create new column "FamilySize" & "Alone"
    # Parch          /
    # Ticket         removed
    # Fare           fill na with median
    # Cabin          removed
    # Embarked       one-hot, fill empty with mode (most common value "S")

    br = '\n'

    # drop unused column
    drop_columns = ['Cabin', 'Ticket']
    df = df.drop(drop_columns, axis=1)

    # fill na
    df['Age'].fillna(df['Age'].mean(), inplace=True)
    df['Fare'].fillna(df['Fare'].median(), inplace=True)
    df['Embarked'].fillna(df['Embarked'].mode()[0], inplace = True)

    # special treatment
    df['FamilySize'] = df['SibSp'] + df['Parch'] + 1
    df['Alone']=0
    df.loc[df['FamilySize']==0,'Alone'] = 1
    #df["Pclass"] = df.astype(str)

    # name --> create new column title. Method copied from web
    # lower the score! so omitted
    #df['Title']=0
    #for i in df:
    #    df['Title']=df.Name.str.extract('([A-Za-z]+)\.')
    #df['Title'].replace(['Countess','Lady','Mme'],'Mrs',inplace=True)
    #df['Title'].replace(['Mlle','Ms'],'Miss',inplace=True)
    #df['Title'].replace('Sir','Mr',inplace=True)
    #df['Title'].replace(['Capt','Col','Don','Dona','Dr','Jonkheer','Major','Rev'],'Other',inplace=True)
    #df['Title'].replace(['Master','Miss','Mr','Mrs','Other'],[0,1,2,3,4],inplace=True)
    df = df.drop('Name', axis=1)

    # age --> fill na by group. Method copied from web
    # mean values come from grouped mean of age by Pclass and Title
    # complicated method, not much difference, ignored
    #df.loc[(df['Pclass']==1)&(df['Title']==0)&(df['Age'].isnull()),'Age']=6.98
    #df.loc[(df['Pclass']==1)&(df['Title']==1)&(df['Age'].isnull()),'Age']=30.25
    #df.loc[(df['Pclass']==1)&(df['Title']==2)&(df['Age'].isnull()),'Age']=41.5
    #df.loc[(df['Pclass']==1)&(df['Title']==3)&(df['Age'].isnull()),'Age']=42.86
    #df.loc[(df['Pclass']==1)&(df['Title']==4)&(df['Age'].isnull()),'Age']=48.53
    #df.loc[(df['Pclass']==2)&(df['Title']==0)&(df['Age'].isnull()),'Age']=2.76
    #df.loc[(df['Pclass']==2)&(df['Title']==1)&(df['Age'].isnull()),'Age']=20.87
    #df.loc[(df['Pclass']==2)&(df['Title']==2)&(df['Age'].isnull()),'Age']=32.35
    #df.loc[(df['Pclass']==2)&(df['Title']==3)&(df['Age'].isnull()),'Age']=33.52
    #df.loc[(df['Pclass']==2)&(df['Title']==4)&(df['Age'].isnull()),'Age']=40.7
    #df.loc[(df['Pclass']==3)&(df['Title']==0)&(df['Age'].isnull()),'Age']=6.23
    #df.loc[(df['Pclass']==3)&(df['Title']==1)&(df['Age'].isnull()),'Age']=17.47
    #df.loc[(df['Pclass']==3)&(df['Title']==2)&(df['Age'].isnull()),'Age']=28.32
    #df.loc[(df['Pclass']==3)&(df['Title']==3)&(df['Age'].isnull()),'Age']=32.33
    #df = df.drop('Title', axis=1)   # dropped is better


    # before one-hot
    df["Pclass"] = df.astype(str)   # turn str before DictVectorizer
    onehot_columns = ['Pclass', 'Sex', 'Embarked']
    org_index = df.index
    onehot_dict = df[onehot_columns].to_dict(orient='records')  
    dv = DictVectorizer(sparse=False, dtype=int)
    encode_np = dv.fit_transform(onehot_dict)
    encode_order = dv.get_feature_names()
    encode_df = pd.DataFrame(encode_np, columns=encode_order)
    encode_df['idx'] = org_index
    encode_df.set_index('idx', inplace=True, drop=True)
    encode_df.index.name = None

    df = df.drop(onehot_columns, axis=1)
    df_result = pd.concat([df, encode_df], axis=1)

    return df_result


def detect_outliers(df, n, features):
    '''
    Remove outliers by IQR
    '''
    outlier_indices=[]
    for col in features:
        Q1 = np.percentile(df[col],25)
        Q3 = np.percentile(df[col],75)
        IQR = Q3 - Q1
        outlier_list_col = df[(df[col] < Q1 - 1.5 * IQR)|(df[col] > Q3 + 1.5 * IQR)].index
        outlier_indices.extend(outlier_list_col)
    outlier_indices = Counter(outlier_indices)
    multiple_outliers=list(k for k,v in outlier_indices.items() if v>n)
    return multiple_outliers


def get_scores(model, X_train, y_train, X_test, y_test):
    y_pred = model.predict(X_test)
    train = model.score(X_train, y_train)
    test = model.score(X_test, y_test)
    name = model.__class__.__name__
    return (name, train, test, y_pred)


def get_csv(result, col_id, fname="model"):
    y_pred = pd.DataFrame(col_id)
    y_pred['Survived'] = pd.DataFrame(data=result, columns=['Survived'])
    y_pred.to_csv(fname + '_submit.csv', sep=',', index=False)


if __name__ == "__main__":

    # read data
    br = '\n'
    titanic_train = pd.read_csv('data/train.csv')
    titanic_pred = pd.read_csv('data/test.csv')

    X = titanic_train.drop(['PassengerId', 'Survived'], axis=1)
    y = titanic_train['Survived']

    X_pred = titanic_pred.drop(['PassengerId'], axis=1)
    pred_col = titanic_pred['PassengerId']

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)

    # omit outliers
    outliers = detect_outliers(X_train, 2, ['Age', 'SibSp', 'Parch', 'Fare'])
    X_train = X_train.drop(outliers, axis=0)
    y_train = y_train.drop(outliers, axis=0)

    # transform dataset
    X_train_tfm = transform_data(X_train, verbose=True)
    X_test_tfm = transform_data(X_test)
    X_pred_tfm = transform_data(X_pred)

    # train: RandomForestClassifier
    rf = RandomForestClassifier(random_state=0, n_estimators=100)
    params = {'class_weight': ['balanced'], 'max_depth': [10, 30]}
    random = RandomizedSearchCV(rf, param_distributions = params, cv=3, n_iter=2, random_state=0)
    random.fit(X_train_tfm, y_train)  
    bp = random.best_params_
    rf_est = random.best_estimator_
    rf = RandomForestClassifier(**bp, random_state=0, n_estimators=100)
    rf.fit(X_train_tfm, y_train)
    rf_scores = get_scores(rf, X_train_tfm, y_train, X_test_tfm, y_test)
    print (rf_scores[0] + ' (train, test):')    # name
    print (rf_scores[1], rf_scores[2], br)      # train, test scores

    y_pred = rf.predict(X_pred_tfm)
    get_csv(y_pred, pred_col, rf_scores[0])


    # train: ExtraTreesClassifier
    et = ExtraTreesClassifier(random_state=0, n_estimators=100)
    params = {'class_weight': ['balanced'], 'max_depth': [10, 30]}
    random = RandomizedSearchCV(et, param_distributions = params, cv=3, n_iter=2, random_state=0)
    random.fit(X_train_tfm, y_train)  
    bp = random.best_params_
    et_est = random.best_estimator_
    et = ExtraTreesClassifier(**bp, random_state=0, n_estimators=200)
    et.fit(X_train_tfm, y_train)
    et_scores = get_scores(et, X_train_tfm, y_train, X_test_tfm, y_test)
    print (et_scores[0] + ' (train, test):')    # name
    print (et_scores[1], et_scores[2], br)      # train, test scores

    y_pred = et.predict(X_pred_tfm)
    get_csv(y_pred, pred_col, et_scores[0])

    # logistic
    param_grid = {'penalty': ['l2'], 'solver': ['newton-cg', 'lbfgs', 'sag'],
                  'max_iter': [4000], 'multi_class': ['auto'], 'C': [0.001, 0.01, 0.1]}
    lr = LogisticRegression(random_state=0, solver='lbfgs', multi_class='auto', max_iter=4000)
    grid = GridSearchCV(lr, param_grid, cv=5, n_jobs=-1)
    grid.fit(X_train_tfm, y_train)
    bp = grid.best_params_
    lr_est = grid.best_estimator_
    lr = LogisticRegression(**bp, random_state=0)
    lr.fit(X_train_tfm, y_train)
    lr_scores = get_scores(lr, X_train_tfm, y_train, X_test_tfm, y_test)
    print (lr_scores[0] + ' (train, test):')    # name
    print (lr_scores[1], lr_scores[2], br)      # train, test scores

    y_pred = lr.predict(X_pred_tfm)
    get_csv(y_pred, pred_col, lr_scores[0])


    # GradientBoostingClassifier
    param_grid = {'n_estimators': range(100,500,100), 'loss': ['deviance'],
                  'learning_rate':[0.0001,0.001,0.01,0.1]}
    gbc = GradientBoostingClassifier(random_state=0)
    grid = GridSearchCV(gbc, param_grid, cv=5, n_jobs=-1)
    grid.fit(X_train_tfm, y_train)
    bp = grid.best_params_
    gbc_est = grid.best_estimator_
    gbc = GradientBoostingClassifier(**bp, random_state=0)
    gbc.fit(X_train_tfm, y_train)
    gbc_scores = get_scores(gbc, X_train_tfm, y_train, X_test_tfm, y_test)
    print (gbc_scores[0] + ' (train, test):')    # name
    print (gbc_scores[1], gbc_scores[2], br)      # train, test scores

    y_pred = gbc.predict(X_pred_tfm)
    get_csv(y_pred, pred_col, gbc_scores[0])



    # logistic + scaler
    scaler = StandardScaler()
    X_train_std = scaler.fit_transform(X_train_tfm)
    X_test_std = scaler.fit_transform(X_test_tfm)
    param_grid = {'penalty': ['l2'], 'solver': ['newton-cg', 'lbfgs', 'sag'],
                  'max_iter': [4000], 'multi_class': ['auto'], 'C': [0.001, 0.01, 0.1]}
    lr_std = LogisticRegression(random_state=0, solver='lbfgs', multi_class='auto', max_iter=4000)
    grid = GridSearchCV(lr, param_grid, cv=5, n_jobs=-1)
    grid.fit(X_train_std, y_train)
    bp = grid.best_params_
    lr_std_est = grid.best_estimator_
    lr_std = LogisticRegression(**bp, random_state=0)
    lr_std.fit(X_train_std, y_train)
    lr_std_scores = get_scores(lr_std, X_train_tfm, y_train, X_test_tfm, y_test)
    print ('Std_lg' + ' (train, test):')        # name
    print (lr_std_scores[1], lr_std_scores[2], br)      # train, test scores

    y_pred = lr_std.predict(X_pred_tfm)
    get_csv(y_pred, pred_col, 'lr_std')

    # VotingClassifier
    voting = VotingClassifier(estimators=[('rf', rf_est),
                                        #('et', et_est),
                                        ('gbc', gbc_est),
                                        ('lr', lr_est)
                                        #('lr_std', lr_std_est)
                                        ],
                            voting='soft')
    voting = voting.fit(X_train_tfm, y_train)

    voting_scores = get_scores(voting, X_train_tfm, y_train, X_test_tfm, y_test)
    print (voting_scores[0] + ' (train, test):')        # name
    print (voting_scores[1], voting_scores[2], br)      # train, test scores

    y_pred = voting.predict(X_pred_tfm)
    get_csv(y_pred, pred_col, voting_scores[0])




