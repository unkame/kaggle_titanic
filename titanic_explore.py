'''
Kaggle - Titanic case
'''

import numpy as np, pandas as pd
import matplotlib.pyplot as plt

def explore_data(df):
    print(type(df))
    features = list(df)
    print("features: ", len(features), " ", features)     # column headers
    print("data shape: ", df.shape)
    print("train data 1-5 rows: ", df.head(), br)

    #print(df.describe())

    # show first row
    #print(X_train.iloc[0])

    # check missing values of columns
    # age has 177, cabin has 687, embarked
    # decision:
    # age:      average
    # cabin:    value or empty
    # embarked: empty as 1 cat
    print(df.isna().sum())  


if __name__ == "__main__":

    # explore sample data
    br = '\n'
    titanic = pd.read_csv('data/train.csv')
    X_train = titanic.drop(['Survived'], axis=1)
    y_train = titanic['Survived']
    explore_data(X_train)

    X_test = pd.read_csv('data/test.csv')
    explore_data(X_test)


